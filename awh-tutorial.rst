Potential of mean force using AWH method 
=========================================

Here we learn how to calculate the potential of mean force
(PMF) along a reaction coordinate (RC) using the accelerated weight
histogram method (AWH) in GROMACS.  We will go through both how to set
up the input files, as well as how to extract and analyze the
output after the simulation is complete. AWH applies a time-dependent bias
potential along the chosen RC, which is tuned during the simulation such that
it "flattens" the barriers of the PMF to improve sampling along the RC.

{{generic_tutorial_instructions}}

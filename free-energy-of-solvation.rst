Free energy of solvation
========================

Change in free energy is a very important physical quantity that determines the direction
of many biological processes. Free energy perturbation (or alchemical free energy computation)
is a computational technique for evaluating the difference in free energy between two states.
In this tutorial you will learn how to apply this technique to compute the
free energy of solvation of a simple molecule.

{{generic_tutorial_instructions}}

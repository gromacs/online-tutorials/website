# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'GROMACS tutorials'
copyright = '2021-2024, the GROMACS tutorial team'
author = 'Mark Abraham'

# The full version, including alpha/beta/rc tags
release = 'https://tutorials.gromacs.org'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store',
                    # This next file is included via a custom mechanism below.
                    'generic-tutorial-instructions.rst']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'
html_sidebars = {
    '**': [
        'globaltoc.html',
#        'sourcelink.html',
#        'searchbox.html'
    ],
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Generate links with substitution -------------------------------------------------

from pathlib import Path
instructions = Path('generic-tutorial-instructions.rst').read_text()

def rstjinja(app, docname, source):
    """
    Render our pages as a jinja template for fancy templating goodness.
    """

    # First add the generic tutorial instructions
    templates_to_instantiate = {
        'generic_tutorial_instructions': instructions,
    }
    source[0] = app.builder.templates.render_string(
        source[0], templates_to_instantiate
    )

    # Then replace the tutorial name everywhere, including from the
    # generic instructions. This currently relies on the name of the
    # tutorial repository matching the name of the reStructuredText
    # file in this repo, but could be generalized when we have the
    # need.
    templates_to_instantiate = {
        'tutorial_name': docname,
        'branch_name': 'main',
    }
    source[0] = app.builder.templates.render_string(
        source[0], templates_to_instantiate
    )

def setup(app):
    app.connect("source-read", rstjinja)

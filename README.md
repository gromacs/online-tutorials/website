![Build Status](https://gitlab.com/gromacs/online-tutorials/website/badges/main/pipeline.svg)

---

This is the GROMACS tutorial website source repository.
It is written in [reStructuredText] which is a simple plain-text markup language also used for the GROMACS documentation.
Those text files are built with the Python-based [sphinx] documentation builder.
It is deployed automatically as a website to https://tutorials.gromacs.org using [GitLab Pages].

Learn more about GitLab Pages at https://about.gitlab.com/product/pages/ and the official
documentation https://docs.gitlab.com/ee/user/project/pages/.

## Website structure

The website is a simple hierarchy containing a top-level landing page
with static links to a page for the latest version of each tutorial
topic.
GitLab pages publishes this content at https://tutorials.gromacs.org.

![Website site diagram](images/website site diagram.png)

The material for each tutorial has its own project on Gitlab, which
contains a git repository for the content,
e.g. https://gitlab.com/gromacs/online-tutorials/umbrella-sampling.

## Making and testing changes

Changes are made via GitLab branches from which merge requests
are made to the main branch.
Review of the work in progress is integral, and [GitLab CI][ci]
produces drafts of the webpage for review before the merge
request is submitted and the final webpage published.

![Workflow for updating the website](images/development workflow.png)

Changes cannot be pushed directly to the `main` branch.
Instead, checkout a new local branch, make your changes there.
You can do a local build if you have [Sphinx][sphinx] installed, or
you can push to GitLab using that branch name and it will build your
changes as a draft.
Then automatically [GitLab CI][ci] will try to build a draft of the
new website via a CI/CD pipeline, and make it visible (but not
published to the public URL yet).
When you're ready for others to review, make a merge request to `main`
branch.

This visible form can be found in several ways, but perhaps
easiest is to start from
https://gitlab.com/gromacs/online-tutorials/website/-/pipelines.
Find the "test" stage, click through to it, find the "Job artifacts"
and browse them.
Click through the file structure from "public" to "index.html" and you
will be at the landing page as if the tip of the current branch was
published as the website page.

Once happy with the result, you or another team member can submit
the change.
The changed branch will be automatically published to the public URL.

### Making a new tutorial

There is a template repository for new GROMACS tutorials at
https://gitlab.com/gromacs/online-tutorials/templates/tutorial. 
To use it,

1.  Go to https://gitlab.com/gromacs/online-tutorials and click on the blue `New project` button.
2.  Choose `Create from template` 
3.  Switch from the `Built-in` tab to the `Group` tab to see the template for GROMACS tutorials
4.  Choose `Use template`
5.  Name your tutorial project something related to its content. Please avoid naming it with your name, or the name of an organization with when you are affiliated. Please avoid using the words "GROMACS" or "tutorial" since that is already known from the location of the tutorial.
6.  Add a description of the tutorial. Here it is appropriate to mention who created or supports maintaining the tutorials.
7.  Change the visibility level to `Public`
8.  Choose `Create project`
9.  Add your tutorial content to the git repository for the new tutorial project.
    In the `README.md` file, change `TUTORIAL_NAME` to the GitLab project name so that the links will be correct.
    Change the `TUTORIAL TITLE` and `SHORT TUTORIAL DESCRIPTION` to content suitable for your tutorial.
10. Change the default conda environment name in the `environment.yml` file from `gromacs-tutorials` to the name of your tutorial

Note only group owner can create a new project.  

### Adding links to a new tutorial

Make a new top-level `.rst` file, named for the new tutorial.
Follow the example of the existing tutorial `.rst` files, by
describing the expected learning outcomes directly connected
to the topic.
Make sure the final line looks like
`{{generic_tutorial_instructions}}` (see below for what is going on
here).
Update the table of contents in `index.rst` to list the
new tutorial and add the plain text of your tutorial (tutorial_name.rst) in the docs directory.
Put all of that in a git commit on a new local branch, and
follow the instructions above.

### Updating an existing tutorial

Typically all updates will go in the repository for the project
for that tutorial, not this one.
Only the latest version of each tutorial is made readily available
on the website.

## File structure

There is a top-level `index.rst` page that contains a table of
contents and list of tutorials, and a separate page for each tutorial.

Each tutorial needs very repetitive
content for generating links and giving instructions to users.
When we want to make changes to that content, we want it to
be applied to all the tutorials automatically.
The native reStructuredText mechanism for this is not powerful enough,
so we use the widely used Python Jinja2 templates to add this content.
Luckily this is also used by Sphinx, so it's already available to us.
The repetitive content from `generic-tutorial-instructions.rst`
is inserted wherever `{{generic_tutorial_instructions}}` is found.
When you see `{{text_like_this}}` you know that some text replacement
is going on, **before** Sphinx handles the reStructuredText.

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

`conf.py` describes how we want [Sphinx][sphinx] to do its job,
including the Jinja2 customization described above.

Images used on the website are found in the `images` folder.
These can include `.drawio` files that can be edited online at
https://app.diagrams.net/ or offline using their tools, or perhaps
your own SVG editor of choice.

## Requirements

- [Sphinx][] (which incorporates Jinja2)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][sphinx] Sphinx
1. Generate the documentation: `make`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

## Troubleshooting

No issues reported yet.

---

Forked from https://gitlab.com/pages/sphinx

[ci]: https://about.gitlab.com/product/continuous-integration/
[reStructuredText]: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
[sphinx]: http://www.sphinx-doc.org/
[GitLab Pages]: https://about.gitlab.com/product/pages/

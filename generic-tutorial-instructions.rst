The tutorial is provided as an interactive Jupyter notebook based on GROMACS.
You can run the tutorial online in your browser without
installing anything, or download it to run locally with your
own installed software.

To see the tutorial as plain text click :doc:`here <docs/{{tutorial_name}}>`

Running this tutorial online
----------------------------

These tutorials are provided online for you for free thanks to `mybinder.org`_ and their sponsors.

.. _mybinder.org: https://mybinder.org/

Click to `run a free online interactive binder at MyBinder.org
<https://mybinder.org/v2/gl/gromacs%2Fonline-tutorials%2F{{tutorial_name}}/{{branch_name}}?filepath=tutorial.ipynb>`_.
It will take several minutes to launch.

Alternatively, click to `run a free online interactive binder at BioExcel-Binder <https://bioexcel-binder.tsi.ebi.ac.uk/v2/gl/gromacs%2Fonline-tutorials%2F{{tutorial_name}}/{{branch_name}}?filepath=tutorial.ipynb>`_. Note that a GitHub account is required to access BioExcel-Binder.

Running the tutorial offline
----------------------------

You will need to install some software on the terminal command line to be able to run the tutorial offline.
Jupyter notebooks are built on Python, and the GROMACS tutorial content needs various other Python packages as well.
Please follow the instructions below for your operating system to get a suitable terminal.

For MacOS users
^^^^^^^^^^^^^^^

You can use the standard Terminal app.
You can launch a terminal via spotlight search, either click the magnifying glass icon in the top-left corner or press the `Command-space` key combination.
Then type "Terminal" and press Return, and a terminal window will appear for you to use.

For Windows users
^^^^^^^^^^^^^^^^^

We strongly recommend to use (and install if necessary) the Windows Subsystem for Linux, WSL.
Inside it, you will need Python 3 and the conda Python environment manager.
A useful guide to doing this is found at https://github.com/kapsakcj/win10-linux-conda-how-to. 

For Linux users
^^^^^^^^^^^^^^^

You can use the standard terminal.
Press the 'Ctrl-Alt-T` key combination and a terminal window will appear for you to use.

For all users once you have a terminal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First, install miniconda for Python 3 by following 
https://docs.conda.io/en/latest/miniconda.html#linux-installers 

Then download an archive of the notebook and supporting files from `this link <https://gitlab.com/gromacs/online-tutorials/{{tutorial_name}}/-/archive/{{branch_name}}/{{tutorial_name}}-{{branch_name}}.zip>`_

Save it to e.g. the "Downloads" folder in your home folder.

Open a command-line terminal, make a folder to run the tutorial, and change into it:

.. code-block:: bash

    mkdir {{tutorial_name}}
    cd {{tutorial_name}}

Unpack the files with

.. code-block:: bash

    unzip ~/Downloads/{{tutorial_name}}-{{branch_name}}.zip

If this fails, you may need to install the unzip program, e.g. with `sudo apt-get install unzip` and then try again.

Now, use miniconda to make the environment for the GROMACS tutorials with:

.. code-block:: bash

    # For all users, Linux, MacOS, and Windows users
    conda env create --name {{tutorial_name}} --file environment.yml

Activate it with:

.. code-block:: bash

    conda activate {{tutorial_name}}

Finally, you can launch the Jupyter notebook for the tutorial in your browser with

.. code-block:: bash

    jupyter-notebook tutorial.ipynb

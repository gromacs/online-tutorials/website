Introduction to Membrane-Protein Simulation  
===============================================================

Here we learn step-by-step how to run a molecular dynamics simulation of a simple membrane protein using GROMACS. We will go through both how to set up the input files, as well as how to set up all the steps from energy minimization to data production  Please reference this tutorial as doi:10.5281/zenodo.10952993

{{generic_tutorial_instructions}}


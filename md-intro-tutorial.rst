Introduction to Molecular Dynamics  
===================================

Here we learn step-by-step how to run a molecular dynamics simulation of a small protein in water solution using GROMACS. We will go through both how to set up the input files, as well as how to set up energy minimization and simulations at constant temperature and at constant pressure. Finally we briefly introduce some of the GROMACS tools for analyzing molecular simulation trajectories. Please reference this tutorial as doi:10.5281/zenodo.11198375

{{generic_tutorial_instructions}}


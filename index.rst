.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the GROMACS tutorials!
====================================================

This is the home of the free online `GROMACS`_ tutorials.
The tutorials are provided as interactive Jupyter notebooks.
This is the same content regularly used in training workshops around GROMACS.
You can download the notebooks to run on your own computer, or run them
right now in your browser, thanks to `mybinder.org`_.

Jupyter notebooks are a popular way to provide online interactive computational content.
If they are new for you, then perhaps you would like to check out a `quick
Youtube video
<https://www.youtube.com/watch?v=jZ952vChhuI&ab_channel=MichaelFudge>`_. Or in alternative have a look at the
learning pathway: `Using Jupyter Notebooks for biomolecular research <https://cms.competency.ebi.ac.uk/learning-pathway/using-jupyter-notebooks-biomolecular-research>`_.

Available tutorials
-------------------

* :doc:`md-intro-tutorial`
* :doc:`membrane-protein`  
* :doc:`umbrella-sampling`
* :doc:`free-energy-of-solvation`
* :doc:`awh-tutorial`
* :doc:`awh-free-energy-of-solvation`
* :doc:`density-fit-simulation`

.. toctree::
   :hidden:
   :maxdepth: 1

   md-intro-tutorial
   membrane-protein
   umbrella-sampling
   free-energy-of-solvation
   awh-tutorial
   awh-free-energy-of-solvation
   density-fit-simulation
   Back to GROMACS page <http://www.gromacs.org> 

Licensing
---------

All the GROMACS tutorials available from this site are freely licensed under the `CC-BY-4.0`_
You are welcome to use them in any way, download them for personal use or use in your own teaching.
Please acknowledge the funding agencies who supported this work and the people who developed the content.

.. Sponsors
   --------
   The tutorial developers are supported by two projects with a mission
   to deliver high-quality training in molecular simulations, namely
   `BioExcel`_ and `ENCCS`_. Funding from the European Commission and the
   Swedish Research Council is gratefully acknowledged.
   .. image:: images/Bioexcel.png
      :height: 180 px
      :align: center
   .. image:: images/ENCCS.png
      :height: 100 px
      :align: center

Other GROMACS training resources
--------------------------------

* Justin Lemkul's GROMACS tutorials: http://www.mdtutorials.com/gmx/
* GROMACS tutorials by Wes Barnett and Vedran Miletić: https://group.miletic.net/en/tutorials/gromacs/
* Mapping computation to HPC hardware & GPU accelerators and heterogeneous architectures by Szilárd Páll and Berk Hess https://doi.org/10.6084/m9.figshare.22303477  
* GROMACS GPU performance by Mark Abraham: https://enccs.github.io/gromacs-gpu-performance/
* GROMACS-CP2K QMMM tutorial by Dmitry Morozov:
  https://github.com/bioexcel/gromacs-2022-cp2k-tutorial
* BioBB GROMACS-related workflows by Adam Hospital on Protein MD Setup: https://mmb.irbbarcelona.org/biobb/workflows#gromacs-protein-md-setup Protein-ligand MD Setup: https://mmb.irbbarcelona.org/biobb/workflows#gromacs-protein-ligand-complex-md-setup
  

.. _GROMACS: https://www.gromacs.org/
.. _Jupyter: https://jupyter.org/
.. _mybinder.org: https://mybinder.org/
.. _BioExcel: https://bioexcel.eu
.. _ENCCS: https://enccs.se
.. _CC-BY-4.0: https://creativecommons.org/licenses/by/4.0/

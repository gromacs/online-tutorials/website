Density-fit simulation  
===================================

Here we learn step-by-step how to run a density-fit simulation of a protein in water solution using GROMACS. In particular we density fitting to fit the structure of the G-protein coupled calcitonin receptor-like receptor (CLR) to its cryo-EM density. 
We will go through first how to set up the molecular system and the density map, then how to set up density-guided simulations. Finally, we briefly introduce some strategy for analyzing the quality of the fitted structure. Please reference this tutorial as `doi:10.5281/zenodo.13938906 <https://doi.org/10.5281/zenodo.13938906>`_

{{generic_tutorial_instructions}}

